import pytest


def pytest_addoption(parser):
    parser.addoption("--service-url", action="store")


@pytest.fixture(scope="session")
def service_url(request):
    service_url_value = request.config.option.service_url
    return service_url_value
