package gitlab

import (
	"gitlab.apertis.org/infrastructure/authz-mediator/claims"
)

type Gitlab struct{}

func (gl *Gitlab) Apply(claims *claims.Claims) {}
