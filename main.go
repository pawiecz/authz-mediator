package main

import (
	"context"
	"flag"
	"log"
	"net/http"
	"os"

	"github.com/coreos/go-oidc/v3/oidc"

	"gitlab.apertis.org/infrastructure/authz-mediator/config"
	"gitlab.apertis.org/infrastructure/authz-mediator/endpoints"
	"gitlab.apertis.org/infrastructure/authz-mediator/mediator"
)

var (
	configFile string
)

func setFlags() {
	flag.StringVar(&configFile, "config", "", "config file path")
}

func main() {
	mediatorSecret, ok := os.LookupEnv("SECRET")
	if !ok {
		mediatorSecret = "not-so-secret"
	}

	setFlags()
	flag.Parse()

	c := &config.Config{}
	if configFile != "" {
		var err error
		if c, err = config.GetConfig(configFile); err != nil {
			log.Fatalf("failed to get config from file %s: %v", configFile, err)
		}
	}
	c.ApplyEnv()
	c.ApplyDefaults()

	httpClient := http.DefaultClient
	ctx := oidc.ClientContext(context.Background(), httpClient)
	provider, err := oidc.NewProvider(ctx, c.Provider)
	if err != nil {
		log.Fatalf("failed to query provider %q: %v", c.Provider, err)
		return
	}
	endpoints, err := endpoints.NewEndpoints(c.Provider, httpClient)
	if err != nil {
		log.Fatalf("failed to query provider %q for endpoints: %v", c.Provider, err)
		return
	}

	verifier := provider.Verifier(&oidc.Config{ClientID: c.ID})
	med := mediator.NewMediator(mediatorSecret, httpClient, verifier, endpoints, c)

	host, err := med.Host()
	if err != nil {
		log.Fatalf("failed to get listen address: %s", err)
	}

	http.HandleFunc("/auth", med.AuthHandler)
	http.HandleFunc("/token", med.TokenHandler)
	http.HandleFunc("/keys", med.PubKeysHandler)
	http.HandleFunc("/userinfo", med.UserInfoHandler)
	http.HandleFunc("/callback", med.CallbackHandler)
	log.Fatal(http.ListenAndServe(host, nil))
}
