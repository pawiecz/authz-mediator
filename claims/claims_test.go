package claims_test

import (
	"github.com/coreos/go-oidc/v3/oidc"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"gitlab.apertis.org/infrastructure/authz-mediator/claims"
)

var _ = Describe("Claims", func() {
	Describe("Extract", func() {
		var (
			c   *claims.Claims
			err error
		)

		BeforeEach(func() {
			c, err = claims.Extract(&oidc.IDToken{})
		})

		It("should fail and return no data for malformed ID token", func() {
			Expect(err).To(HaveOccurred())
			Expect(err.Error()).To(ContainSubstring("error decoding ID token claims:"))
			Expect(c).To(BeNil())
		})
	})
})
