package claims

import (
	"fmt"

	"github.com/coreos/go-oidc/v3/oidc"
)

type Claims struct {
	Name   string   `json:"name"`
	Email  string   `json:"email"`
	Groups []string `json:"groups"`
}

func Extract(token *oidc.IDToken) (*Claims, error) {
	var claims Claims
	if err := token.Claims(&claims); err != nil {
		return nil, fmt.Errorf("error decoding ID token claims: %v", err)
	}
	return &claims, nil
}
