module gitlab.apertis.org/infrastructure/authz-mediator

go 1.16

require (
	github.com/coreos/go-oidc/v3 v3.0.0
	github.com/onsi/ginkgo v1.16.4
	github.com/onsi/gomega v1.13.0
	golang.org/x/oauth2 v0.0.0-20210615190721-d04028783cf1
	gopkg.in/yaml.v2 v2.4.0
)
