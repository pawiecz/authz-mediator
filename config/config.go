package config

import (
	"fmt"
	"io/ioutil"
	"os"

	"gopkg.in/yaml.v2"
)

const (
	DefaultMediatorID  = "authz-mediator"
	DefaultProviderURL = "http://localhost:5556/dex"
	DefaultMediatorURL = "http://localhost:5555"
	DefaultRedirectURL = "http://localhost/users/auth/openid_connect/callback"

	MediatorIDEnv  = "AUTHZ_MEDIATOR_ID"
	ProviderURLEnv = "AUTHZ_MEDIATOR_PROVIDER"
	MediatorURLEnv = "AUTHZ_MEDIATOR_SELF"
	RedirectURLEnv = "AUTHZ_MEDIATOR_REDIRECT"
)

type Config struct {
	ID       string `yaml:"id"`
	Provider string `yaml:"provider"`
	Mediator string `yaml:"mediator"`
	Redirect string `yaml:"redirect"`
}

func GetConfig(file string) (*Config, error) {
	data, err := ioutil.ReadFile(file)
	if err != nil {
		return nil, fmt.Errorf("failed to read config file %s: %v", file, err)
	}

	var c Config
	err = yaml.Unmarshal(data, &c)
	if err != nil {
		return nil, fmt.Errorf("failed to parse config file %s: %v", file, err)
	}
	return &c, nil
}

func (c *Config) ApplyEnv() {
	if id, ok := os.LookupEnv(MediatorIDEnv); ok {
		c.ID = id
	}
	if provider, ok := os.LookupEnv(ProviderURLEnv); ok {
		c.Provider = provider
	}
	if mediator, ok := os.LookupEnv(MediatorURLEnv); ok {
		c.Mediator = mediator
	}
	if redirect, ok := os.LookupEnv(RedirectURLEnv); ok {
		c.Redirect = redirect
	}
}

func (c *Config) ApplyDefaults() {
	if c.ID == "" {
		c.ID = DefaultMediatorID
	}
	if c.Provider == "" {
		c.Provider = DefaultProviderURL
	}
	if c.Mediator == "" {
		c.Mediator = DefaultMediatorURL
	}
	if c.Redirect == "" {
		c.Redirect = DefaultRedirectURL
	}
}
