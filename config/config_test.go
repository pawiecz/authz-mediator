package config_test

import (
	"os"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"gitlab.apertis.org/infrastructure/authz-mediator/config"
)

var _ = Describe("Config", func() {
	const (
		foo = "foo"
		bar = "bar"
		baz = "baz"
		q2z = "quuz"
	)

	var (
		conf *config.Config
	)

	BeforeEach(func() {
		conf = &config.Config{}
	})

	Describe("ApplyEnv", func() {
		var (
			sampleConfig *config.Config
		)

		BeforeEach(func() {
			sampleConfig = &config.Config{
				ID:       q2z,
				Provider: foo,
				Mediator: bar,
				Redirect: baz,
			}
		})

		Context("when there is no relevant configuration set in env", func() {
			Context("when config is empty", func() {
				BeforeEach(func() {
					conf.ApplyEnv()
				})

				It("should leave config unchanged", func() {
					Expect(conf).To(Equal(&config.Config{}))
				})
			})

			Context("when config is set", func() {
				BeforeEach(func() {
					conf = &config.Config{
						ID:       q2z,
						Provider: foo,
						Mediator: bar,
						Redirect: baz,
					}
					conf.ApplyEnv()
				})

				It("should leave config unchanged", func() {
					Expect(conf).To(Equal(sampleConfig))
				})
			})
		})

		Context("when there is relevant configuration set in env", func() {
			BeforeEach(func() {
				os.Setenv(config.MediatorIDEnv, q2z)
				os.Setenv(config.ProviderURLEnv, foo)
				os.Setenv(config.MediatorURLEnv, bar)
				os.Setenv(config.RedirectURLEnv, baz)
				conf.ApplyEnv()
			})

			AfterEach(func() {
				os.Unsetenv(config.MediatorIDEnv)
				os.Unsetenv(config.ProviderURLEnv)
				os.Unsetenv(config.MediatorURLEnv)
				os.Unsetenv(config.RedirectURLEnv)
			})

			Context("when config is empty", func() {
				It("should apply config from env", func() {
					Expect(conf).To(Equal(sampleConfig))
				})
			})

			Context("when config is set", func() {
				BeforeEach(func() {
					conf = &config.Config{
						ID:       baz,
						Provider: bar,
						Mediator: foo,
						Redirect: q2z,
					}
					Expect(conf).ToNot(Equal(sampleConfig))
					conf.ApplyEnv()
				})

				It("should apply config from env", func() {
					Expect(conf).To(Equal(sampleConfig))
				})
			})
		})
	})

	Describe("ApplyDefaults", func() {
		Context("when config is missing fields", func() {
			BeforeEach(func() {
				conf.ApplyDefaults()
			})

			It("should fill config with defaults", func() {
				Expect(conf.ID).To(Equal(config.DefaultMediatorID))
				Expect(conf.Provider).To(Equal(config.DefaultProviderURL))
				Expect(conf.Mediator).To(Equal(config.DefaultMediatorURL))
				Expect(conf.Redirect).To(Equal(config.DefaultRedirectURL))
			})
		})

		Context("when config is already filled out", func() {
			BeforeEach(func() {
				conf.ID = q2z
				conf.Provider = foo
				conf.Mediator = bar
				conf.Redirect = baz
				conf.ApplyDefaults()
			})

			It("should leave config unchanged", func() {
				Expect(conf.ID).To(Equal(q2z))
				Expect(conf.Provider).To(Equal(foo))
				Expect(conf.Mediator).To(Equal(bar))
				Expect(conf.Redirect).To(Equal(baz))
			})
		})
	})
})
