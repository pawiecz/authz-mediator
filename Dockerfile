FROM golang:1.16.5-alpine3.13 AS builder

WORKDIR /usr/local/src/authz-mediator

COPY go.mod go.sum ./
RUN go mod download

COPY . .

RUN go install

FROM alpine:3.13.5

COPY --from=builder /go/bin/authz-mediator /usr/local/bin/authz-mediator

USER 1001:1001

CMD ["authz-mediator"]
