package endpoints

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

type Endpoints struct {
	Auth     string `json:"authorization_endpoint"`
	Token    string `json:"token_endpoint"`
	PubKeys  string `json:"jwks_uri"`
	UserInfo string `json:"userinfo_endpoint"`
}

func NewEndpoints(providerURL string, client *http.Client) (*Endpoints, error) {
	wellKnown := strings.TrimSuffix(providerURL, "/") + "/.well-known/openid-configuration"
	req, err := http.NewRequest(http.MethodGet, wellKnown, nil)
	if err != nil {
		return nil, err
	}

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("failed to read response body: %v", err)
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("%s: %s", resp.Status, body)
	}

	var e Endpoints
	err = json.Unmarshal(body, &e)
	if err != nil {
		return nil, fmt.Errorf("failed to unmarshal provider discovery object: %v", err)
	}

	return &e, nil
}
