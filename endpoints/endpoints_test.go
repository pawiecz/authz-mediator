package endpoints_test

import (
	"fmt"
	"net/http"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"github.com/onsi/gomega/ghttp"

	"gitlab.apertis.org/infrastructure/authz-mediator/endpoints"
)

var _ = Describe("Endpoints", func() {
	Describe("NewEndpoints", func() {
		const (
			path = "/.well-known/openid-configuration"
		)

		var (
			server *ghttp.Server
		)

		BeforeEach(func() {
			server = ghttp.NewServer()
		})

		AfterEach(func() {
			server.Close()
		})

		Context("when provider URI is not parsable", func() {
			It("should return a parsing error", func() {
				e, err := endpoints.NewEndpoints("\r\n", http.DefaultClient)
				Expect(err).To(HaveOccurred())
				Expect(err.Error()).To(Equal(fmt.Sprintf("parse \"\\r\\n%s\": net/url: invalid control character in URL", path)))
				Expect(e).To(BeNil())
			})
		})

		Context("when discovery request cannot be made", func() {
			It("should return a request error", func() {
				host := "foo"
				service := fmt.Sprintf("https://%s", host)
				e, err := endpoints.NewEndpoints(service, http.DefaultClient)
				Expect(err).To(HaveOccurred())
				Expect(err.Error()).To(HavePrefix(fmt.Sprintf("Get \"%s%s\": dial tcp: lookup %s", service, path, host)))
				Expect(err.Error()).To(HaveSuffix("no such host"))
				Expect(e).To(BeNil())
			})
		})

		Context("when response status is not OK", func() {
			It("should return an error response from server", func() {
				server.AppendHandlers(
					ghttp.CombineHandlers(
						ghttp.VerifyRequest(http.MethodGet, path),
						ghttp.RespondWith(http.StatusBadRequest, "not OK"),
					),
				)
				e, err := endpoints.NewEndpoints(server.URL(), http.DefaultClient)
				Expect(err).To(HaveOccurred())
				Expect(err.Error()).To(Equal("400 Bad Request: not OK"))
				Expect(e).To(BeNil())
			})
		})

		Context("when response cannot be unmarshalled", func() {
			It("should return an unmarshalling error", func() {
				server.AppendHandlers(
					ghttp.CombineHandlers(
						ghttp.VerifyRequest(http.MethodGet, path),
						ghttp.RespondWithJSONEncoded(http.StatusOK, "foo"),
					),
				)
				e, err := endpoints.NewEndpoints(server.URL(), http.DefaultClient)
				Expect(err).To(HaveOccurred())
				Expect(err.Error()).To(ContainSubstring("failed to unmarshal provider discovery object:"))
				Expect(e).To(BeNil())
			})
		})

		Context("when proper request is made", func() {
			It("should return endpoints and no error", func() {
				eps := &endpoints.Endpoints{
					Auth:     "foo",
					Token:    "bar",
					PubKeys:  "baz",
					UserInfo: "quuz",
				}
				server.AppendHandlers(
					ghttp.CombineHandlers(
						ghttp.VerifyRequest(http.MethodGet, path),
						ghttp.RespondWithJSONEncoded(http.StatusOK, eps),
					),
				)
				e, err := endpoints.NewEndpoints(server.URL(), http.DefaultClient)
				Expect(err).ToNot(HaveOccurred())
				Expect(e).To(Equal(eps))
			})
		})
	})
})
