package mediator

import (
	"context"
	"fmt"
	"net/http"
	"net/url"

	"github.com/coreos/go-oidc/v3/oidc"

	"gitlab.apertis.org/infrastructure/authz-mediator/config"
	"gitlab.apertis.org/infrastructure/authz-mediator/endpoints"
	"gitlab.apertis.org/infrastructure/authz-mediator/gitlab"
)

type Mediator struct {
	id          string
	secret      string
	selfURL     string
	endpoints   *endpoints.Endpoints
	redirectURL string

	client   *http.Client
	verifier MediatorVerifier

	membership *gitlab.Gitlab
}

type MediatorVerifier interface {
	Verify(ctx context.Context, rawIDToken string) (*oidc.IDToken, error)
}

func NewMediator(secret string, client *http.Client, verifier MediatorVerifier, e *endpoints.Endpoints, c *config.Config) *Mediator {
	return &Mediator{
		id:          c.ID,
		secret:      secret,
		selfURL:     c.Mediator,
		endpoints:   e,
		redirectURL: c.Redirect,
		client:      client,
		verifier:    verifier,
	}
}

func (med *Mediator) Host() (string, error) {
	selfURL, err := url.Parse(med.selfURL)
	if err != nil {
		return "", fmt.Errorf("failed to parse mediator URL: %s", err)
	}
	return selfURL.Host, nil
}
