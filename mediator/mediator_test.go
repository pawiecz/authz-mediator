package mediator_test

import (
	"context"
	"encoding/json"
	"io"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"

	"github.com/coreos/go-oidc/v3/oidc"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"github.com/onsi/gomega/ghttp"

	"gitlab.apertis.org/infrastructure/authz-mediator/config"
	"gitlab.apertis.org/infrastructure/authz-mediator/endpoints"
	"gitlab.apertis.org/infrastructure/authz-mediator/mediator"
)

type FakeVerifier struct{}

func (f FakeVerifier) Verify(ctx context.Context, rawIDToken string) (*oidc.IDToken, error) {
	return &oidc.IDToken{}, nil
}

var _ = Describe("Mediator", func() {
	const (
		mediatorID  = "glhf"
		mediatorURL = "https://apert.is:1337"
		providerURL = "https://oidc.ftw"
		redirectURL = "https://phone.home:82526"
		notSecret   = "n0ts3kr3t"

		authRoute     = "/auth"
		tokenRoute    = "/token"
		pubKeysRoute  = "/keys"
		userInfoRoute = "/userinfo"

		stateArg = "f0071e55"
		nonceArg = "5ca1ab1e"
		scopeArg = "foo bar baz"
		codeArg  = "deadc0de"
	)

	var (
		med *mediator.Mediator
		e   *endpoints.Endpoints
		c   *config.Config

		rr      *httptest.ResponseRecorder
		req     *http.Request
		vals    *url.Values
		handler http.HandlerFunc
	)

	BeforeEach(func() {
		e = &endpoints.Endpoints{
			Auth:     providerURL + authRoute,
			Token:    providerURL + tokenRoute,
			PubKeys:  providerURL + pubKeysRoute,
			UserInfo: providerURL + userInfoRoute,
		}
		c = &config.Config{
			ID:       mediatorID,
			Provider: providerURL,
			Mediator: mediatorURL,
			Redirect: redirectURL,
		}
		med = mediator.NewMediator(notSecret, http.DefaultClient, &FakeVerifier{}, e, c)

		rr = httptest.NewRecorder()
		req = &http.Request{}
		vals = &url.Values{}
		handler = http.NotFound
	})

	Describe("AuthHandler", func() {
		BeforeEach(func() {
			vals.Add(mediator.StateParam, stateArg)
			vals.Add(mediator.NonceParam, nonceArg)
			vals.Add(mediator.ScopeParam, scopeArg)
		})

		Context("when request URI is not parsable", func() {
			BeforeEach(func() {
				// httptest.NewRequest panics on unparsable URIs
				req, err := http.NewRequest(http.MethodGet, "", nil)
				Expect(err).ToNot(HaveOccurred())
				handler = http.HandlerFunc(med.AuthHandler)
				handler.ServeHTTP(rr, req)
			})

			It("should return internal server error status code", func() {
				Expect(rr.Code).To(Equal(http.StatusInternalServerError))
				Expect(rr.Body.String()).To(Equal("parse \"\": empty url\n"))
			})
		})

		Context("when request is missing state", func() {
			BeforeEach(func() {
				uri, err := url.Parse(mediatorURL + authRoute)
				Expect(err).ToNot(HaveOccurred())

				vals.Del(mediator.StateParam)
				uri.RawQuery = vals.Encode()

				req = httptest.NewRequest(http.MethodGet, uri.String(), nil)
				handler = http.HandlerFunc(med.AuthHandler)
				handler.ServeHTTP(rr, req)
			})

			It("should return bad request status code", func() {
				Expect(rr.Code).To(Equal(http.StatusBadRequest))
				Expect(rr.Body.String()).To(Equal("missing state param\n"))
			})
		})

		Context("when request is missing nonce", func() {
			BeforeEach(func() {
				uri, err := url.Parse(mediatorURL + authRoute)
				Expect(err).ToNot(HaveOccurred())

				vals.Del(mediator.NonceParam)
				uri.RawQuery = vals.Encode()

				req = httptest.NewRequest(http.MethodGet, uri.String(), nil)
				handler = http.HandlerFunc(med.AuthHandler)
				handler.ServeHTTP(rr, req)
			})

			It("should return bad request status code", func() {
				Expect(rr.Code).To(Equal(http.StatusBadRequest))
				Expect(rr.Body.String()).To(Equal("missing nonce param\n"))
			})
		})

		Context("when request is missing scope", func() {
			BeforeEach(func() {
				uri, err := url.Parse(mediatorURL + authRoute)
				Expect(err).ShouldNot(HaveOccurred())

				vals.Del(mediator.ScopeParam)
				uri.RawQuery = vals.Encode()

				req = httptest.NewRequest(http.MethodGet, uri.String(), nil)
				handler = http.HandlerFunc(med.AuthHandler)
				handler.ServeHTTP(rr, req)
			})

			It("should return bad request status code", func() {
				Expect(rr.Code).To(Equal(http.StatusBadRequest))
				Expect(rr.Body.String()).To(Equal("missing scope param\n"))
			})
		})

		Context("when proper request is made", func() {
			var (
				location *url.URL
				query    url.Values
			)

			BeforeEach(func() {
				uri, err := url.Parse(mediatorURL + authRoute)
				Expect(err).ToNot(HaveOccurred())

				uri.RawQuery = vals.Encode()

				req = httptest.NewRequest(http.MethodGet, uri.String(), nil)
				handler = http.HandlerFunc(med.AuthHandler)
				handler.ServeHTTP(rr, req)

				location, err = rr.Result().Location()
				Expect(err).ToNot(HaveOccurred())
				query = location.Query()
			})

			It("should return see other status code", func() {
				Expect(rr.Code).To(Equal(http.StatusSeeOther))
				location, err := rr.Result().Location()
				Expect(err).ShouldNot(HaveOccurred())
				Expect(location.String()).To(ContainSubstring(c.Provider + authRoute))
			})

			It("should contain client ID", func() {
				arg := query.Get("client_id")
				Expect(arg).To(Equal(c.ID))
			})

			It("should contain non-empty state argument", func() {
				arg := query.Get(mediator.StateParam)
				Expect(arg).ToNot(BeEmpty())
			})

			It("should contain non-empty nonce argument", func() {
				arg := query.Get(mediator.NonceParam)
				Expect(arg).ToNot(BeEmpty())
			})
		})
	})

	Describe("TokenHandler", func() {
		var (
			reader io.Reader
		)

		BeforeEach(func() {
			vals.Add(mediator.CodeParam, codeArg)
			vals.Add(mediator.ScopeParam, scopeArg)
			reader = strings.NewReader(vals.Encode())
		})

		Context("when OIDC request is missing code", func() {
			BeforeEach(func() {
				vals.Del(mediator.CodeParam)
				reader = strings.NewReader(vals.Encode())
				req = httptest.NewRequest(http.MethodPost, mediatorURL+tokenRoute, reader)
				req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
				handler = http.HandlerFunc(med.TokenHandler)
				handler.ServeHTTP(rr, req)
			})

			It("should return bad request error status code", func() {
				Expect(rr.Code).To(Equal(http.StatusBadRequest))
				Expect(rr.Body.String()).To(Equal("missing code param\n"))
			})
		})

		Context("when OIDC request is missing scope", func() {
			BeforeEach(func() {
				vals.Del(mediator.ScopeParam)
				reader = strings.NewReader(vals.Encode())
				req = httptest.NewRequest(http.MethodPost, mediatorURL+tokenRoute, reader)
				req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
				handler = http.HandlerFunc(med.TokenHandler)
				handler.ServeHTTP(rr, req)
			})

			It("should return bad request error status code", func() {
				Expect(rr.Code).To(Equal(http.StatusBadRequest))
				Expect(rr.Body.String()).To(Equal("missing scope param\n"))
			})
		})

		Context("when OIDC request cannot be made", func() {
			BeforeEach(func() {
				c.Provider = "" // request will return an error without protocol scheme
				med = mediator.NewMediator(notSecret, http.DefaultClient, &FakeVerifier{}, e, c)
				req = httptest.NewRequest(http.MethodPost, mediatorURL+tokenRoute, reader)
				req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
				handler = http.HandlerFunc(med.TokenHandler)
				handler.ServeHTTP(rr, req)
			})

			It("should return internal server error status code", func() {
				Expect(rr.Code).To(Equal(http.StatusInternalServerError))
				Expect(rr.Body.String()).To(ContainSubstring("failed to exchange code for token:"))
			})
		})

		Context("when proper request is made", func() {
			var (
				server *ghttp.Server
				tokens *mediator.TokenResponse
			)

			BeforeEach(func() {
				tokens = &mediator.TokenResponse{
					TokenType:   "bearer",
					AccessToken: "granted",
					IDToken:     "unknown",
					ExpiresIn:   31337,
				}

				server = ghttp.NewServer()
				server.AppendHandlers(
					ghttp.CombineHandlers(
						ghttp.VerifyRequest(http.MethodPost, "/"),
						ghttp.RespondWithJSONEncoded(http.StatusOK, tokens),
					),
				)
				e.Token = server.URL()
				med = mediator.NewMediator(notSecret, http.DefaultClient, &FakeVerifier{}, e, c)

				req = httptest.NewRequest(http.MethodPost, mediatorURL+tokenRoute, reader)
				req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
				handler = http.HandlerFunc(med.TokenHandler)
				handler.ServeHTTP(rr, req)
			})

			AfterEach(func() {
				server.Close()
			})

			It("should return OK status code", func() {
				Expect(server.ReceivedRequests()).To(HaveLen(1))
				Expect(rr.Code).To(Equal(http.StatusOK))
			})

			It("should pass server response unchanged", func() {
				Expect(server.ReceivedRequests()).To(HaveLen(1))
				tokens.ExpiresIn -= 1 // unless server response is near instantenous MatchJSON below will have to be reimplemented to fuzzy-match expiry time
				tokenJSON, err := json.Marshal(tokens)
				Expect(err).ToNot(HaveOccurred())
				Expect(rr.Body.String()).To(MatchJSON(tokenJSON))
			})
		})
	})

	Describe("PubKeysHandler", func() {
		BeforeEach(func() {
			req = httptest.NewRequest(http.MethodGet, mediatorURL+pubKeysRoute, nil)
			handler = http.HandlerFunc(med.PubKeysHandler)
			handler.ServeHTTP(rr, req)
		})

		It("should redirect to the provider public keys route", func() {
			Expect(rr.Code).To(Equal(http.StatusSeeOther))
			location, err := rr.Result().Location()
			Expect(err).ToNot(HaveOccurred())
			Expect(location.String()).To(Equal(c.Provider + pubKeysRoute))
		})
	})

	Describe("UserInfoHandler", func() {
		Context("when request URI is not parsable", func() {
			BeforeEach(func() {
				e.UserInfo = "\r\n"
				med = mediator.NewMediator(notSecret, http.DefaultClient, &FakeVerifier{}, e, c)
				req = httptest.NewRequest(http.MethodGet, mediatorURL+userInfoRoute, nil)
				handler = http.HandlerFunc(med.UserInfoHandler)
				handler.ServeHTTP(rr, req)
			})

			It("should return internal server error status code", func() {
				Expect(rr.Code).To(Equal(http.StatusInternalServerError))
				Expect(rr.Body.String()).To(Equal("parse \"\\r\\n\": net/url: invalid control character in URL\n"))
			})
		})

		Context("when OIDC request cannot be made", func() {
			BeforeEach(func() {
				e.UserInfo = "" // request will return an error without protocol scheme
				med = mediator.NewMediator(notSecret, http.DefaultClient, &FakeVerifier{}, e, c)
				req = httptest.NewRequest(http.MethodGet, mediatorURL+userInfoRoute, nil)
				handler = http.HandlerFunc(med.UserInfoHandler)
				handler.ServeHTTP(rr, req)
			})

			It("should return internal server error status code", func() {
				Expect(rr.Code).To(Equal(http.StatusInternalServerError))
				Expect(rr.Body.String()).To(Equal("Get \"\": unsupported protocol scheme \"\"\n"))
			})
		})

		Context("when proper request is made", func() {
			const (
				headerKey   = "X-OIDC-Test"
				headerValue = "oidc-test"
			)

			var (
				server   *ghttp.Server
				userInfo map[string]string
			)

			BeforeEach(func() {
				userInfo = map[string]string{
					"email":          "foo@bar.biz",
					"email_verified": "true",
				}

				server = ghttp.NewServer()
				server.AppendHandlers(
					ghttp.CombineHandlers(
						ghttp.VerifyRequest(http.MethodGet, "/"),
						ghttp.VerifyHeader(http.Header{headerKey: []string{headerValue}}),
						ghttp.RespondWithJSONEncoded(http.StatusOK, userInfo),
					),
				)
				e.UserInfo = server.URL()
				med = mediator.NewMediator(notSecret, http.DefaultClient, &FakeVerifier{}, e, c)

				req = httptest.NewRequest(http.MethodGet, mediatorURL+userInfoRoute, nil)
				req.Header.Add(headerKey, headerValue)
				handler = http.HandlerFunc(med.UserInfoHandler)
				handler.ServeHTTP(rr, req)
			})

			AfterEach(func() {
				server.Close()
			})

			It("should return OK status code", func() {
				Expect(server.ReceivedRequests()).To(HaveLen(1))
				Expect(rr.Code).To(Equal(http.StatusOK))
			})

			It("should pass server response unchanged", func() {
				Expect(server.ReceivedRequests()).To(HaveLen(1))
				userInfoJSON, err := json.Marshal(userInfo)
				Expect(err).ToNot(HaveOccurred())
				Expect(rr.Body.String()).To(BeEquivalentTo(userInfoJSON))
			})
		})
	})

	Describe("CallbackHandler", func() {
		const (
			queryParam1 = "foo"
			queryParam2 = "bar"
			queryArg1   = "baz"
			queryArg2   = "quuz"
		)

		Context("when request URI is not parsable", func() {
			BeforeEach(func() {
				// httptest.NewRequest panics on unparsable URIs
				req, err := http.NewRequest(http.MethodGet, "", nil)
				Expect(err).ToNot(HaveOccurred())
				handler = http.HandlerFunc(med.CallbackHandler)
				handler.ServeHTTP(rr, req)
			})

			It("should return internal server error status code", func() {
				Expect(rr.Code).To(Equal(http.StatusInternalServerError))
				Expect(rr.Body.String()).To(Equal("parse \"\": empty url\n"))
			})
		})

		Context("when redirect URL is not parsable", func() {
			BeforeEach(func() {
				c.Redirect = "\r\n"
				med = mediator.NewMediator(notSecret, http.DefaultClient, &FakeVerifier{}, e, c)
				req = httptest.NewRequest(http.MethodGet, mediatorURL+mediator.CallbackRoute, nil)
				handler = http.HandlerFunc(med.CallbackHandler)
				handler.ServeHTTP(rr, req)
			})

			It("should return internal server error status code", func() {
				Expect(rr.Code).To(Equal(http.StatusInternalServerError))
				Expect(rr.Body.String()).To(Equal("parse \"\\r\\n\": net/url: invalid control character in URL\n"))
			})
		})

		Context("when proper request is made", func() {
			var (
				location *url.URL
				query    url.Values
			)

			BeforeEach(func() {
				uri, err := url.Parse(mediatorURL + mediator.CallbackRoute)
				Expect(err).ToNot(HaveOccurred())

				q := &url.Values{}
				q.Add(queryParam1, queryArg1)
				q.Add(queryParam2, queryArg2)
				uri.RawQuery = q.Encode()

				req = httptest.NewRequest(http.MethodGet, uri.String(), nil)
				handler = http.HandlerFunc(med.CallbackHandler)
				handler.ServeHTTP(rr, req)

				location, err = rr.Result().Location()
				Expect(err).ToNot(HaveOccurred())
				query = location.Query()
			})

			It("should return see other status code", func() {
				Expect(rr.Code).To(Equal(http.StatusSeeOther))
				location, err := rr.Result().Location()
				Expect(err).ShouldNot(HaveOccurred())
				Expect(location.String()).To(ContainSubstring(c.Redirect))
			})

			It("should contain request query", func() {
				queryMap := map[string]string{
					queryParam1: queryArg1,
					queryParam2: queryArg2,
				}

				for p, a := range queryMap {
					arg := query.Get(p)
					Expect(arg).To(Equal(a))
				}
			})
		})
	})

	Describe("Host", func() {
		var (
			host string
			err  error
		)

		Context("when SelfURL is not parsable", func() {
			BeforeEach(func() {
				c.Mediator = "\r\n"
				med = mediator.NewMediator(notSecret, http.DefaultClient, &FakeVerifier{}, e, c)
				host, err = med.Host()
			})

			It("should return an empty string and an error", func() {
				Expect(host).To(BeEmpty())
				Expect(err).To(HaveOccurred())
				Expect(err.Error()).To(ContainSubstring("failed to parse mediator URL:"))
			})
		})

		Context("when proper SelfURL is used", func() {
			BeforeEach(func() {
				host, err = med.Host()
			})

			It("should return some string and no error", func() {
				Expect(host).ToNot(BeEmpty())
				Expect(err).ToNot(HaveOccurred())
			})

			It("should return a string of two parts separated by a single colon", func() {
				parts := strings.Split(host, ":")
				Expect(len(parts)).To(Equal(2))
			})
		})
	})
})
