package mediator

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/coreos/go-oidc/v3/oidc"
	"golang.org/x/oauth2"

	"gitlab.apertis.org/infrastructure/authz-mediator/claims"
)

const (
	CallbackRoute = "/callback"

	StateParam = "state"
	NonceParam = "nonce"
	ScopeParam = "scope"
	CodeParam  = "code"
)

func (med *Mediator) AuthHandler(w http.ResponseWriter, r *http.Request) {
	requestURI, err := url.ParseRequestURI(r.RequestURI)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	query := requestURI.Query()
	state := query.Get(StateParam)
	if state == "" {
		http.Error(w, fmt.Sprintf("missing %s param", StateParam), http.StatusBadRequest)
		return
	}
	nonce := query.Get(NonceParam)
	if nonce == "" {
		http.Error(w, fmt.Sprintf("missing %s param", NonceParam), http.StatusBadRequest)
		return
	}
	scope := query.Get(ScopeParam)
	if scope == "" {
		http.Error(w, fmt.Sprintf("missing %s param", ScopeParam), http.StatusBadRequest)
		return
	}

	redirectURL := med.oauth2Config(strings.Split(scope, " ")).AuthCodeURL(state, oauth2.SetAuthURLParam(NonceParam, nonce))
	http.Redirect(w, r, redirectURL, http.StatusSeeOther)
}

func (med *Mediator) TokenHandler(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	code := r.FormValue(CodeParam)
	if code == "" {
		http.Error(w, fmt.Sprintf("missing %s param", CodeParam), http.StatusBadRequest)
		return
	}
	scope := r.FormValue(ScopeParam)
	if scope == "" {
		http.Error(w, fmt.Sprintf("missing %s param", ScopeParam), http.StatusBadRequest)
		return
	}

	token, err := med.exchange(r.Context(), code, strings.Split(scope, " "))
	if err != nil {
		http.Error(w, fmt.Sprintf("failed to exchange code for token: %v", err), http.StatusInternalServerError)
		return
	}

	idToken, err := med.verifier.Verify(r.Context(), token.IDToken)
	if err != nil {
		http.Error(w, fmt.Sprintf("failed to verify token: %v", err), http.StatusInternalServerError)
		return
	}

	claims, err := claims.Extract(idToken)
	if err == nil {
		go med.membership.Apply(claims) // nothing to apply if claims extraction failed; still, should not affect tokens relay
	}

	jsonToken, err := json.Marshal(token)
	if err != nil {
		http.Error(w, fmt.Sprintf("failed to marshal token: %v", err), http.StatusInternalServerError)
		return
	}
	fmt.Fprintf(w, "%s", jsonToken)
}

func (med *Mediator) PubKeysHandler(w http.ResponseWriter, r *http.Request) {
	http.Redirect(w, r, med.endpoints.PubKeys, http.StatusSeeOther)
}

func (med *Mediator) UserInfoHandler(w http.ResponseWriter, r *http.Request) {
	req, err := http.NewRequest(http.MethodGet, med.endpoints.UserInfo, nil)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	req.Header = r.Header

	resp, err := med.client.Do(req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	fmt.Fprintf(w, "%s", body)
}

func (med *Mediator) CallbackHandler(w http.ResponseWriter, r *http.Request) {
	requestURI, err := url.ParseRequestURI(r.RequestURI)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	redirectURL, err := url.Parse(med.redirectURL)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	redirectURL.RawQuery = requestURI.Query().Encode()
	http.Redirect(w, r, redirectURL.String(), http.StatusSeeOther)
}

func (med *Mediator) oauth2Config(scope []string) *oauth2.Config {
	return &oauth2.Config{
		ClientID:     med.id,
		ClientSecret: med.secret,
		RedirectURL:  med.selfURL + CallbackRoute,
		Endpoint: oauth2.Endpoint{
			AuthURL:   med.endpoints.Auth,
			TokenURL:  med.endpoints.Token,
			AuthStyle: 0,
		},
		Scopes: scope,
	}
}

func (med *Mediator) exchange(parentCtx context.Context, code string, scope []string) (*TokenResponse, error) {
	ctx := oidc.ClientContext(parentCtx, med.client)
	oauth2Config := med.oauth2Config(scope)

	token, err := oauth2Config.Exchange(ctx, code)
	if err != nil {
		return nil, fmt.Errorf("failed to get token: %v", err)
	}

	rawIDToken, ok := token.Extra("id_token").(string)
	if !ok {
		return nil, fmt.Errorf("no id_token in token response")
	}

	return toTokenResponse(rawIDToken, token.AccessToken, token.Expiry), nil
}

type TokenResponse struct {
	TokenType   string `json:"token_type"`
	AccessToken string `json:"access_token"`
	IDToken     string `json:"id_token"`
	ExpiresIn   int    `json:"expires_in"`
}

func toTokenResponse(idToken, accessToken string, expiry time.Time) *TokenResponse {
	return &TokenResponse{
		"bearer",
		accessToken,
		idToken,
		int(time.Until(expiry).Seconds()),
	}
}
